from django.db import models
from event.models import Event

# Create your models here.
class FBUser(models.Model):
	username  			= models.CharField(max_length=255,primary_key=True)
	email 				= models.EmailField(max_length=200)
	first_name			= models.CharField(max_length=200)
	last_name			= models.CharField(max_length=200)
	user_image	 		= models.CharField(max_length=255)     #gets User image url from facebook
	friend_count 		= models.IntegerField()
	profile_link 		= models.URLField(max_length=255)
	user_id 			= models.CharField(max_length=255)
	access_token 		= models.TextField(max_length=255)
	supported_events 	= models.ManyToManyField(Event)
	created_events 		= models.TextField(blank=True)

	def show_supported(self):
		return self.supported_events.all()

	def show_created(self):
		new_str = []
		event_list = []
		new_str = self.created_events.split(',')
		new_str.pop()
		for each_pk in new_str:
			event_list.append(Event.objects.get(pk = int(each_pk)))
		return event_list

	def __unicode__(self):
		return self.username
