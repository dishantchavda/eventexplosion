from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin
from views import *
from django.conf.urls.static import static
import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'eventexplosion.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^events/', 'eventexplosion.views.category'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^login/$', login),
    #url(r'^accounts/profile/', TemplateView.as_view(template_name='index.html' )),
    url(r'^accounts/profile/', 'eventexplosion.views.show_profile'),

    #event app urls are going to be here on
    # url(r'^event/$','event.views.events'),     									#matches : http://localhost/events/
    url(r'^event/add/$','event.views.new_event'),		                        #matches : http://localhost/events/add/
    # url(r'^event/(?P<event_slug>[a-zA-Z0-9_.-]+)/$','event.views.detail'),		#matches : http://localhost/events/event-slug/
    url(r'^event/support/(?P<event_slug>[a-zA-Z0-9_.-]+)/$','event.views.support'),
    #User authentication urls
    url(r'^register/$','event.views.user_registration'),
    #url(r'^login/$','event.views.login_user'),
    url(r'^logout/$','event.views.logout_user'),
    url(r'^profile/$','event.views.profile'),
) + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
