from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
from allauth.socialaccount.models import SocialToken, SocialAccount
from django.contrib.auth import authenticate,login,logout
from eventuser.models import FBUser
import facebook
import settings

def getFriendCount(access_token):
	graph = facebook.GraphAPI(access_token)
	profile = graph.get_object("me")
	friends = graph.get_connections("me", "friends")
	#print profile['name']
	#print friends
	return len(friends['data'])

def post(access_token):
	graph = facebook.GraphAPI(access_token)
	graph.put_object("me", "feed", message="New Facebook graph API is just awesome to manipulate....")

def home(request):
	context = {'title':'EventExplosion','eventname':'Chaos','user':request.user}
	return render(request,'index.html',context)

# def category(request):
# 	a = request.get_full_path()
# 	category = a[len('/events/'):]
# 	# html = get_template('category.html').render(Context({'title':'category'}))
# 	#return HttpResponse(html)
# 	return render_to_response( 'category.html', )

#request.user.socialaccount_set.filter(provider='facebook')[0].get_avatar_url
#t = SocialToken.objects.filter(account__user=request.user, account__provider='facebook') #returns a list of ST objects

def show_profile(request):
	#sa = SocialAccount.objects.get(user = request.user)  #Gives Error at start some time so using alternate method
	n = request.user.socialaccount_set.filter(provider='facebook')[0]
	t = n.socialtoken_set.all()
	#im = "https://graph.facebook.com/"+ n.uid +"/picture?width=9999&height=9999"    	     #returns facebook pic url
	im = "https://graph.facebook.com/"+ n.uid +"/picture?type=large"
	p = n.extra_data        	    	 #returns dictionary of all user data
	try:
		new = FBUser.objects.get(username = p['username'])
	except Exception:
		new = FBUser.objects.create(username = p['username'],email = p['email'],first_name = p['first_name'],last_name = p['last_name'],user_image = im,friend_count = getFriendCount(t[0].token),profile_link = p['link'],user_id = n.uid,access_token = t[0].token)
	context = {'myuser': new}
	return 	render(request,'profile.html',context)


