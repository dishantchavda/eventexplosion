#Import Utilities to render requested pages
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
#Import Models and forms for template context passing
from event.models import Event, EventUser
from event.forms import EventFilling, RegistrationForm, LoginForm
#Import User login Logout utilities
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
"""
Changes TO be Made :
	- Use request.get_full_path() to get prev URL for current redirect
	- Use HttpResponseRedirect and reverse to show better redirects for profile page redirects
	- make page contexts global so we can edit them at a place
"""

def events(request):
	"""Main page view of EventExplosion site
	passes all the Events to the template"""
	supported_events = []
	if request.user.is_authenticated():
		current_user = EventUser.objects.get(user = request.user)
		supported_events = current_user.show_supported()
	list_events = Event.objects.all()
	context = {'events' : list_events,'supported_events' : supported_events}
	return render(request,'events/events.html',context)

def detail(request, event_slug):
	"""Show user Detailed view of event he selected
	User may support that event on this page"""
	supported_events = []
	if request.user.is_authenticated():
		current_user = EventUser.objects.get(user = request.user)
		supported_events = current_user.show_supported()
	event = Event.objects.get(slug = event_slug)
	return render(request,'events/detail.html', {'event' : event,'supported_events' : supported_events})

@login_required
def new_event(request):
	"""Redirects to form to add new event
	If posting a form it saves form data and redirects to that event
	Does validation of data first
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/login/')"""
	if request.method == 'POST':
	#Submitting a form save if valid
		f = EventFilling(request.POST,request.FILES)
		if f.is_valid():
			#Form validation done save the user entries
			poster = EventUser.objects.get(user = request.user)
			p = Event.objects.create(event_name = f.cleaned_data['event_name'],catagory = f.cleaned_data['catagory'],short_description = f.cleaned_data['short_description'],long_description = f.cleaned_data['long_description'],supporter_goal = f.cleaned_data['supporter_goal'],completion_date = f.cleaned_data['completion_date'],event_creator = poster.display_name,event_image = f.cleaned_data['event_image'],link = f.cleaned_data['link'],hashtag = f.cleaned_data['hashtag'],event_state = 'O')
			poster.created_events = poster.created_events + str(p.pk) + ','
			poster.save()
			#return user to newly created event
			return render(request,'events/detail.html', {'event' : p})
			#Do smt so that url changes with redirect so need to use below Http method
			#return HttpResponseRedirect(reverse('event.views.detail', args=str(p.slug)))
		else:
			#form validation failed send user back with error form
			return 	render(request,'events/new_event.html',{'form':f})
	else:
		#Show a new form to user
		form = EventFilling()
		return render(request,'events/new_event.html',{'form':form})

@login_required
def support(request,event_slug):
	s = Event.objects.get(slug = event_slug)
	#we got supporting event in s only if authentic user in it
	u = EventUser.objects.get(user = request.user)
	u.supported_events.add(s)
	supported_events = u.show_supported()
	return render(request,'events/detail.html',{'event':s,'supported_events' : supported_events})

"""User registration / Logout / Login and profile views"""

def user_registration(request):
	if request.user.is_authenticated():
		#User already loged in redirect to profile
		newuser = EventUser.objects.get(user = request.user)
		return render(request,'user/profile.html',{'eventuser':newuser})
	if request.method == 'POST':
		#form posting validate data and save if valid
		f = RegistrationForm(request.POST)
		if f.is_valid():
			user = User.objects.create_user(username = f.cleaned_data['username'],email = f.cleaned_data['email'],password = f.cleaned_data['password'])
			user.save()
			newuser = EventUser(user=user,display_name=f.cleaned_data['display_name'],friend_count=f.cleaned_data['friend_count'],access_token=f.cleaned_data['access_token'])
			newuser.save()
			login(request,user)
			return render(request,'user/profile.html',{'eventuser':newuser})   #Use reverse and Http for url---------
		else:
			#Validation error
			return render(request,'user/register.html',{'form':f})
	else:
		#display form to user for registration
		form = RegistrationForm()
		context  = {'form' : form}
		return render(request,'user/register.html',context)

def login_user(request):
	if request.user.is_authenticated():
		#User already loged in redirect to profile
		newuser = EventUser.objects.get(user = request.user)
		return render(request,'user/profile.html',{'eventuser':newuser})
	if request.method == 'POST':
		f = LoginForm(request.POST)
		if f.is_valid():
			newuser = authenticate(username = f.cleaned_data['username'],password = f.cleaned_data['password'])
			if newuser is not None:
				login(request,newuser)
				n = EventUser.objects.get(user = newuser)
				return render(request,'user/profile.html',{'eventuser':n})
			else:
				return HttpResponseRedirect('/register/')
		else:
			f = LoginForm()
			return render(request,'user/login.html',{'form':f})
	else:
		f = LoginForm()
		return render(request,'user/login.html',{'form':f})

def logout_user(request):
	logout(request)
	return HttpResponseRedirect('/events/')

@login_required
def profile(request):
	#User already loged in redirect to profile
	newuser = EventUser.objects.get(user = request.user)
	return render(request,'user/profile.html',{'eventuser':newuser})

