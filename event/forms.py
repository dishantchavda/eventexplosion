from django.forms import ModelForm
from django import forms
from event.models import Event,EventUser
from django.contrib.auth.models import User

#Main form class to add a new event
class EventFilling(ModelForm):
	class Meta:
		model 	= Event
		exclude = ('event_creator','slug','publish_date','event_state')

#Add new user with this form
class RegistrationForm(ModelForm):
	username		= forms.CharField(label=(u'User Name'))
	email			= forms.EmailField(label=(u'Email Address'))
	password		= forms.CharField(label=(u'Password'),widget=forms.PasswordInput(render_value=False))
	password1		= forms.CharField(label=(u'Retype Password'),widget=forms.PasswordInput(render_value=False))

	class Meta:
		model = EventUser
		exclude = ('user','supported_events','created_events')

	def clean_username(self):
		username = self.cleaned_data['username']
		try:
			User.objects.get(username = username)
		except Exception, e:
			return username
		raise forms.ValidationError('That username is taken, Please Choose another')

	def clean(self):
		if 'password' not in self.cleaned_data and 'password1' not in self.cleaned_data:
			raise forms.ValidationError('Password can not be left blank Noob')
		if self.cleaned_data['password'] != self.cleaned_data['password1']:
			raise forms.ValidationError('Passwords do not match please Type again')
		return self.cleaned_data

#make a login form for the user
class LoginForm(forms.Form):
	username 		= forms.CharField(label=(u'UserName'))
	password 		= forms.CharField(label=(u'password'),widget=forms.PasswordInput(render_value=False))