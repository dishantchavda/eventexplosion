from django.db import models
from django.template.defaultfilters import slugify  #to automatically fill Slug field from name field (uses javascript)
from django.contrib.auth.models import User
from datetime import date

"""
Changes TO be Made :
	- Facebook integration through another app
"""

STATE_LIST = (
	('F','Featured'),
	('C','Completed'),
	('O','OnGoing'),
)

CATAGORY_LIST = (
	('Ca','Cause'),
	('En','Environment'),
	('Te','Technology'),
	('Po','Politics'),
	('Ch','Charity'),
	('Fi','Film'),
	('He','Health'),
	('Pr','Product'),
	('Ar','Art'),
	('En','Entertainment'),
	('Ga','Gamming'),
	('Pt','Protest'),
	('In','Invention'),
)

# Main Event Model
class Event(models.Model):
	event_creator 		= models.CharField(max_length = 255)				#use User display_name
	event_name 			= models.CharField(max_length = 255)
	catagory 			= models.CharField(max_length = 2, choices = CATAGORY_LIST)
	slug 	 			= models.SlugField(unique=True, max_length=255)  	#slug to get events url
	event_image 		= models.ImageField(upload_to='eventimg/')
	short_description 	= models.CharField(max_length = 255)
	long_description 	= models.TextField()
	link 				= models.URLField(max_length = 255 ,blank = True)
	hashtag 			= models.CharField(max_length = 255 ,blank = True)
	supporter_goal 		= models.IntegerField()
	completion_date 	= models.DateField()
	publish_date 		= models.DateField()    							#auto populate when creating event
	event_state 		= models.CharField(max_length = 1, choices = STATE_LIST)

	def get_virality(self):
		"""returns virality value for event when ever called"""
		sum = 0
		supporters = self.eventuser_set.all()
		for supporter in supporters:
			sum = sum + supporter.friend_count
		return sum

	def get_supporter_count(self):
		return len(self.eventuser_set.all())

	def __unicode__(self):
		return self.event_name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.event_name)
		self.publish_date =date.today()
		super(Event, self).save(*args, **kwargs)

#User information
class EventUser(models.Model):
	user  				= models.OneToOneField(User)  #contains username + email + password + firstname + lastname
	display_name 		= models.CharField(max_length=255)
	friend_count 		= models.IntegerField()
	access_token 		= models.CharField(max_length=255)
	supported_events 	= models.ManyToManyField(Event)
	created_events 		= models.TextField(blank=True)

	def show_supported(self):
		return self.supported_events.all()

	def show_created(self):
		new_str = []
		event_list = []
		new_str = self.created_events.split(',')
		new_str.pop()
		for each_pk in new_str:
			event_list.append(Event.objects.get(pk = int(each_pk)))
		return event_list

	def __unicode__(self):
		return self.user.username
